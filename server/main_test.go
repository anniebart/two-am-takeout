package main

import (
	"context"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/url"
	"testing"
	"time"

	"github.com/gorilla/websocket"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"

	"gitlab.com/upchieve/two-am-takeout/server/handlers"
)

var ctx context.Context
var cancel context.CancelFunc

type ServerSuite struct {
	suite.Suite
	ctx    context.Context
	cancel context.CancelFunc
}

func (suite *ServerSuite) SetupSuite() {
	ctx = context.Background()
	suite.ctx, suite.cancel = context.WithCancel(ctx)

	go startServer(ctx)

	time.Sleep(1000)
}

func (suite *ServerSuite) TeardownSuite() {
	suite.cancel()
}

func (suite *ServerSuite) TestHealthCheck() {
	resp, err := http.Get("http://localhost:3000/health")
	assert.Nil(suite.T(), err)
	assert.Equal(suite.T(), http.StatusOK, resp.StatusCode)

	respData, err := ioutil.ReadAll(resp.Body)
	assert.Nil(suite.T(), err)

	var respBody handlers.HealthBody
	err = json.Unmarshal(respData, &respBody)
	assert.Nil(suite.T(), err)

	// make sure health body works matches what the frontend is expecting
	respString := string(respData)
	// below this is where test was failing for first task. 
	assert.Contains(suite.T(), respString, "\"OK\":true")
}
func (suite *ServerSuite) TestWebsockets() {
	u := url.URL{Scheme: "ws", Host: "localhost:3000", Path: "/ws"}

	reader, _, err := websocket.DefaultDialer.Dial(u.String(), nil)
	defer reader.Close()
	assert.Nil(suite.T(), err)

	sender, _, err := websocket.DefaultDialer.Dial(u.String(), nil)
	defer sender.Close()
	assert.Nil(suite.T(), err)

	// checking that text formatted messages go through and can be read correctly
	sender.WriteMessage(websocket.TextMessage, []byte("Hello, Websocket!"))
	_, message, err := reader.ReadMessage()
	assert.Nil(suite.T(), err)
	//I'm thinking that the issue is here. The test passes, but it's not really mimicking how the client would receive the data of a message from the server. 
	// I'm running out of time, so I think the best use, since I'm not familiar with Go, would to go back and focus on the frontend so at least I can showcase my skills there...
	assert.Equal(suite.T(), "Hello, Websocket!", string(message))
}

func TestServerSuite(t *testing.T) {
	suite.Run(t, new(ServerSuite))
}
