This file is for taking notes as you go.

## Annie's notes:
- started this on Wednesday February 24 around 5pm (spent ~10 minutes after call at 3:15pm reading README + forking repo)
- Installed Go
- Issue with starting server, might be issue with Go installation/need to restart VSCode? 
- ^ was an issue with xcode-select, ran 
```
sudo xcode-select --reset
```
and then was able to start server. 

- ran backend tests, looks like first error is just a spelling error (line 55 in test file), fixed that. 
- ran test again, looks like it passes. 
-  for next task on list, skipping for now, will come back to once I implement frontend to make sure I can get that done. 
- poking around Chat.vue file, started development server for front end, looks like tha chat app just adds the text to the top of the page in green bubbles. There is a timeout function printing that the backend check is failing. Re: frontend, will hopefully be able to go back and make a little prettier if time allows. Also would be good to clear input text field after submitting/hitting enter. 
- wondering if for name, rather than just be text field that can be edited everytime, it would probably make more sense for it to display a username (but this would require a sign up/login authentication process which would take much longer than the time allotted.)
- right now, the message will still send if blank, could limit the sending of a message to happen only if a message length > 0. 
- Added in the logic to add username to each message, now figuring out how best to display it with each message. Maybe using a vuetify card? or just appending the text next to the v-chip for now. 
- decided to just do a simple h6 to display username under the chip for now. returning back to task #2, but first taking a break (spent ~50 min on this, + 10 min initially spent)



- Returned back to finish the last hour (Tuesday March 2 @ 11:30am - a little later than I was expecting, got swept away in work last week), goal is to address the second task + also work on cleaning up/making the front end cleaner. Looks like when the chat is sent from a different user, there isn't enough margin or padding from the inputs, so there's an awkward overlap. 
- Looking at the test, wish I was more familiar with Go, I've mostly just stuck with JavaScript as my programming language of choice, so even with Googling syntax, it's a little tricky to decipher it. 
- Think the issue is at line 75 in the test file. I think maybe the assumption is that the message is just a string, rather than a part of an object. So while the strings match and the test passes, it's not really helpful to check that the backend + frontend are communicating properly (the message itself would have to be parsed from the data sent from the server to the client). I'm running out of time and since I'm very unfamiliar with Go, I think the best course of action given the time restraints is to focus on my strengths (JavaScript/frontend stuff) so I'm first going to make sure that the text inputs clear after each message is sent  as well as make sure that blank messages can't be sent. 
- I was going to reset the username after the message is sent, but from a UX perspective, I think it would actually make more sense for it to stay. The user likely wouldn't be changing their name every time they send a message, so it would be annoying if they had to retype their name with every message. 
- Fixed the issue with the overlap of the left aligned messages and the name input. I have about 10 more minutes left to wrap up. Also made sure that users couldn't send blank messages or not input a username
- put the text inputs + buttons on a card for now. It's not ideal, since each sent message bumps it down, which is visually very annoying. 



**If I had more time:**
- Go back and fix test for task #2
- Split up the chat component into smaller components
- Style it more so the text input for the message is positioned in a bar at the bottom with the send button flexed to the right of it (like sending a text on an iPhone etc)
- Figure out a better flow for adding in the username. Maybe adding in a little avatar at the top where users can reset their username their rather than keeping the username input next to the message input. 


